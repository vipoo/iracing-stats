process.env.NODE_PATH = __dirname + '/src:' + __dirname + '/'
require('module').Module._initPaths()

const path = require('path')

require('babel-register')()
require('babel-polyfill')
require('isomorphic-fetch')
const WebpackIsomorphicTools = require('webpack-isomorphic-tools')
const config = require('./config/webpack-isomorphic-tools-configuration').default
const project_base_path = path.resolve(__dirname, './')

global.IntialStoreState = {}
global.ReactEnvironment = { isClient: false, isServer: true }

global.WebpackIsomorphicTools = new WebpackIsomorphicTools(config)
                                      .development(process.env.NODE_ENV !== 'production')
                                      .server(project_base_path, () => require('server'))
