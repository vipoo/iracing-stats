import React from 'react'
import {Grid, Row, Col} from 'react-flexbox-grid'
import { driverSeasonsObservable } from 'stores/driverSeasonsStore'
import { bindToReact } from 'stores/rx_operators'
import { List, ListItem } from 'react-toolbox/lib/list'
import { Card, CardTitle } from 'react-toolbox/lib/card'
import Toolbar from 'components/toolbar'

export default class DriverSeasons extends React.Component {

  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.unsub = driverSeasonsObservable::bindToReact(this)
  }

  componentWillUnmount() {
    this.unsub()
  }

  onClick(s) {
    return () => console.log(s.season_id)
  }

  render() {
    if( !this.state.DriverSeasons)
      return <span/>

    const driverName = this.state.DriverSeasons._links.driver.title
    const seasons = this.state.DriverSeasons._embedded.active_seasons.concat(this.state.DriverSeasons._embedded.inactive_seasons)

    const items = seasons.map(s =>
      <ListItem key={s.season_id}  onClick={::this.onClick(s)} caption={s.series_name + ' ' + s.season_short_name} />)

    return <Grid>
            <Row center='sm' middle="sm">
              <Col xs={12} sm={12}>
                <Card>
                <Toolbar>Seasons</Toolbar>
                <CardTitle title={driverName} />
                  <List selectable ripple>
                    {items}
                  </List>
                </Card>
              </Col>
            </Row>
          </Grid>
  }
}
