export default {
  '/':                          'home',
  '/drivers':                   'drivers',
  '/drivers/:driverId/seasons': 'driverSeasons'
}
