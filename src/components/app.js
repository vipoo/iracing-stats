import React from 'react'
import injectTapEventPlugin from 'react-tap-event-plugin'
import { RouterMixin } from 'react-mini-router'
import Drivers from 'components/driver'
import DriverSeasons from 'components/driverSeasons'
import Home from 'components/home'
import { driverSeasonsSubject } from 'stores/driverSeasonsStore'
import { driversSubject } from 'stores/driversStore'
import routes from 'components/routes'

injectTapEventPlugin()

export default React.createClass({

  mixins: [RouterMixin],

  routes: routes,

  render() {
    return this.renderCurrentRoute()
  },

  home() {
    return <Home/>
  },

  drivers() {
    if(ReactEnvironment.isClient)
      driversSubject.next()

    return <Drivers />
  },

  driverSeasons(driverId) {
    if(ReactEnvironment.isClient)
      driverSeasonsSubject.next(driverId)
    return <DriverSeasons />
  },

  notFound: function(path) {
    return <div className="not-found">Page Not Found: {path}</div>
  }

})

