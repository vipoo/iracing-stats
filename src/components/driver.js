import AutoComplete from 'react-toolbox/lib/autocomplete'
import {Button} from 'react-toolbox/lib/button'
import {Grid, Row, Col} from 'react-flexbox-grid'
import theme from './_driver.scss'
import React from 'react'
import { navigate } from 'react-mini-router'
import { driversObservable } from 'stores/driversStore'
import { bindToReact } from 'stores/rx_operators'

export default class Drivers extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.unsub = driversObservable::bindToReact(this)
  }

  componentWillUnmount() {
    this.unsub()
  }

  handleUpdateInput(driverId) {
    this.setState({driverId})
  }

  showStats() {
    navigate(`/drivers/${this.state.driverId}/seasons`)
  }

  render() {
    return (
      <Grid>
        <Row center='sm' middle="sm">
          <Col xs={12} sm={6}>
            <AutoComplete
              limit={100}
              showSuggestionsWhenValueIsSet={false}
              theme={theme}
              label="Driver Name"
              filter={AutoComplete.caseInsensitiveFilter}
              source={this.state.Drivers}
              onChange={::this.handleUpdateInput}
              multiple={false}
              value={this.state.driverId}
              hint='iracing driver name'
            />
          </Col>
          <Col xs={12} sm={2}>
            <Button label='Show Stats' raised primary onClick={::this.showStats} />
          </Col>
        </Row>
      </Grid>)
  }
}
