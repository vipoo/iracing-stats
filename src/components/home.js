import React from 'react'
import {Grid, Row, Col} from 'react-flexbox-grid'
import { Button } from 'react-toolbox/lib/button'

export default function helloWorld() {
  return <Grid>
    <Row>
      <Col xs={6} md={3}>Hello, world!!!!! <Button label="Default" /> </Col>
    </Row>
  </Grid>
}
