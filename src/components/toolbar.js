import React from 'react'
import theme from './_toolbar.scss'

const Toolbar = props => <div className={theme.toolbar}>{props.children}</div>

export default Toolbar
