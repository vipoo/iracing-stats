import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app'

const reactDiv = document.body.children[1]

ReactDOM.render( <App history={true} />, reactDiv )

