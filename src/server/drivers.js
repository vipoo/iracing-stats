import { driversObservable, driversSubject } from 'stores/driversStore'
import { once } from 'stores/rx_operators'

export default (req, res) => driversObservable
                                ::once(driversSubject)
                                .then(state => res.sendPage(state))
