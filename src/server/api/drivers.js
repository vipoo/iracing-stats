import Rx from '@reactivex/rxjs'
import _ from 'lodash'
import { rxFetch, replayLastValue, consumeWith } from 'stores/rx_operators'

const driversSubject = new Rx.Subject()

const observ = driversSubject::rxFetch()::replayLastValue()
                    .map(drivers =>  drivers._embedded.drivers.map( d => [d.driver_id, d.name] ))
                    .map(drivers => _.sortBy(drivers, kv => kv[1]))
                    .map(Drivers => { return {Drivers} })


const twoMinutes = 2 * 60 * 1000
const serviceEndPoint = 'http://localhost:3080/drivers'
function updateCache() {
  setTimeout(() => driversSubject.next(serviceEndPoint), twoMinutes)
}

observ::consumeWith(() => updateCache())
driversSubject.next(serviceEndPoint)

export default (req, res) => observ.first()::consumeWith(drivers => res.send(drivers))
