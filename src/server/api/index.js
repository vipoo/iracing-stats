import express from 'express'
import drivers from './drivers'
import driverSeasons from './driverSeasons'

const router = express.Router()

router.get('/drivers', drivers)
router.get('/drivers/:driverId/seasons', driverSeasons)

export default router
