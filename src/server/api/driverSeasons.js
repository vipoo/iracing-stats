
//'/drivers/:driverId/seasons

export default (req, res) => {

  const url = `http://localhost:3080/drivers/${req.params.driverId}/seasons`

  return fetch(url)
    .then(r => r.json())
    .then(DriverSeasons => res.send({DriverSeasons}))

}
