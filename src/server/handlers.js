import routes from 'components/routes'
import driverSeasons from './driverSeasons'
import drivers from './drivers'
import home from './home'

const handlers = { home, drivers, driverSeasons }

function get(path, handlerName) {
  const handler = handlers[handlerName]

  this.get(path, (req, res, next) => {
    if(!req.accepts('text/html'))
      return next()

    return handler(req, res, next)
  })
}

export function addRoutes() {
  Object.keys(routes).forEach(k => this::get(k, routes[k]))
}
