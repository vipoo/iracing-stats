import { driverSeasonsObservable, driverSeasonsSubject } from 'stores/driverSeasonsStore'
import { once } from 'stores/rx_operators'

export default (req, res) => driverSeasonsObservable
                                ::once(driverSeasonsSubject, req.params.driverId)
                                .then(state => res.sendPage(state))
