import React from 'react'
import ReactDOMServer from 'react-dom/server'
import path from 'path'
import fs from 'fs'
import ejs from 'ejs'

const webpackAssetsFilePath = path.resolve(__dirname, './../webpack-assets.json')
const indexPath = path.join(__dirname, '..', '..', 'assets', 'index.html.ejs')
const indexHtml = fs.readFileSync(indexPath, { encoding: 'utf-8'})
const template = ejs.compile(indexHtml)
let { styles: { main: cssPath }, javascript: { main: jsPath }} = JSON.parse(fs.readFileSync(webpackAssetsFilePath))

export default function reactServerRender(fn) {

  return (req, res, next) => {
    if( process.env.NODE_ENV !== 'production')
      WebpackIsomorphicTools.refresh()

    res.sendPage = function sendPage(state) {

      try {
        if( process.env.NODE_ENV !== 'production') {
          const assets = JSON.parse(fs.readFileSync(webpackAssetsFilePath))
          cssPath = assets.styles.main
          jsPath = assets.javascript.main
        }

        for(let k of Object.keys(state))
          IntialStoreState[k] = state[k]

        const intialStoreState = 'window.IntialStoreState=' + JSON.stringify(state) + ';'
        const reactString = process.env.NODE_ENV === 'production' ? ReactDOMServer.renderToString(fn(req, res)) : ''
        const html = template({ reactString, jsPath, cssPath, intialStoreState })

        res.set('Content-Type', 'text/html')
        res.send(html)
      }
      catch(e) {
        console.log(e.stack) // eslint-disable-line no-console
      }
    }

    return next()
  }
}
