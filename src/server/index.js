import express from 'express'
import morgan from 'config/morgan'
import api from './api'
import reactServerSide from './react_server_side'
import { addRoutes } from './handlers'
import App from 'components/app'

const app = express()

app.use(express.static('public'))
app.use(morgan)
app.use('/api', api)
app.use(reactServerSide((res, req) => <App path={req.path} />))
app::addRoutes()

app.listen(4081, () => console.log('iracing-stats listening on port 4081')) // eslint-disable-line no-console
