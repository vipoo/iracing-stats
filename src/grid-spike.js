import React from 'react'
import {Grid, Row, Col} from 'react-flexbox-grid'

const style = {
  borderColor: 'red',
  borderWidth: '1px',
  borderStyle: 'solid'
}
export default class AutoCompleteExampleSimple extends React.Component {

  render() {
    return (
      <Grid>
        <Row middle="sm">
          <Col xs={12} sm={6}>
            <div style={style}>COLUMN 1</div>
          </Col>
          <Col xs={12} sm={6}>
            <div style={style}>COLUMN 2</div>
          </Col>
        </Row>

      </Grid>)
  }
}
