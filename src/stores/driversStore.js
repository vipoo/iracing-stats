import Rx from '@reactivex/rxjs'
import { toReactState, consumeWith, retrieveState } from './rx_operators'

const driversSubject = new Rx.Subject()

const driversObservable = driversSubject
         .map(() => 'http://localhost:4081/api/drivers')
        ::retrieveState('Drivers')
        ::toReactState()

driversObservable::consumeWith(() => {})

export { driversSubject, driversObservable }
