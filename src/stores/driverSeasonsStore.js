import Rx from '@reactivex/rxjs'
import { toReactState, consumeWith, retrieveState } from './rx_operators'

const driverSeasonsSubject = new Rx.Subject()

const driverSeasonsObservable = driverSeasonsSubject
         .map(driverId => `http://localhost:4081/api/drivers/${driverId}/seasons`)
        ::retrieveState('DriverSeasons')
        ::toReactState()

driverSeasonsObservable::consumeWith(() => {})

export { driverSeasonsSubject, driverSeasonsObservable }
