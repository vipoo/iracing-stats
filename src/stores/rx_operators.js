import when from 'when'
import Rx from '@reactivex/rxjs'

function _fetch() {
  return fetch(...arguments)
}

export function rxFetch() {
  const subject = new Rx.Subject()

  return Rx.Observable.create(observer => this.subscribe({
                                            next: v => when(_fetch(v))
                                                        .then(r => r.json())
                                                        .then(result => observer.next(result)),
                                            error: err => observer.error(err),
                                            complete: () => observer.complete()
                                          })).multicast(subject).refCount()
}

export function rxFetchWithPreload(key, fetchSubject) {
  const subject = new Rx.Subject()

  const next = (observer, v) => {

    const preloaded = IntialStoreState[key] ? { [key]: IntialStoreState[key] } : undefined
    delete IntialStoreState[key]

    if(preloaded !== undefined) {
      return observer.next(preloaded)
    }

    if(fetchSubject) {
      fetchSubject.next({fetching: v})
    }

    return when(_fetch(v))
            .then(r => r.json())
            .then(result => observer.next(result))
            .then(r => {
              fetchSubject ? fetchSubject.next({fetching: null}) : null
              return r
            })
  }

  return Rx.Observable.create(observer => this.subscribe({
                                            next: v => next(observer, v),
                                            error: err => observer.error(err),
                                            complete: () => observer.complete()
                                          })).multicast(subject).refCount()
}

export function withPreloadedData(key) {

  const input = this

  const preloaded = IntialStoreState[key] ? { [key]: IntialStoreState[key] } : undefined
  delete IntialStoreState[key]

  return Rx.Observable.create(observer => {
    if(preloaded !== undefined)
      observer.next(preloaded)

    return input.subscribe({
      next: v => observer.next(v),
      error: err => observer.error(err),
      complete: () => observer.complete()
    })
  })

}

export function lastValue(emitInitialValue = false) {
  const input = this

  let captured = undefined

  return Rx.Observable.create(observer => {
    if(emitInitialValue || captured) {
      observer.next(captured)
    }

    return input.subscribe({
      next: v => {
        captured = v
        observer.next(v)
      },
      error: err => observer.error(err),
      complete: () => observer.complete()
    })
  })
}

export function replayLastValue() {
  const input = this

  let captured = undefined

  return Rx.Observable.create(observer => {
    if(captured !== undefined)
      observer.next(captured)

    return input.subscribe({
      next: v => {
        captured = v
        observer.next(v)
      },
      error: err => observer.error(err),
      complete: () => observer.complete()
    })
  })
}

export function consumeWith(fn) {
  const o = this.subscribe({next: v => fn(v) } )
  return () => o.unsubscribe()
}

export function once(subject, value) {
  subject.next(value)
  return this.skip(1).first().toPromise()
}

function Server_bindToReact(reactComponent) {
  return this.first()::consumeWith(state => reactComponent.setState(state))
}

function Client_bindToReact(reactComponent) {
  return this::consumeWith(state => reactComponent.setState(state))
}

export const bindToReact = ReactEnvironment.isServer ? Server_bindToReact : Client_bindToReact

export function toReactState() {
  return this::lastValue(ReactEnvironment.isServer)
}

export function clientRetrieveState(name) {

  const getingDataSubject = new Rx.Subject()

  return this::rxFetchWithPreload(name, getingDataSubject)
              .merge(getingDataSubject)

}

export const retrieveState = ReactEnvironment.isServer ? rxFetch : clientRetrieveState

