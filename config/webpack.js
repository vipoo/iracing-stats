import path from 'path'
import autoprefixer from 'autoprefixer'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

const env = process.env.NODE_ENV || 'development'
const name = env === 'development' ? 'dev' : env
const { publicPath, cssLoaders, plugins, devtool } = require('./webpack.' + name)

export default {
  entry:    [
    'babel-polyfill',
    './src/client.js'
  ],

  cache: true,

  context:  path.resolve(__dirname, './../'),
  output: {
    path:           `${__dirname}/../public/assets`,
    filename:       '[name]-[hash].js',
    chunkFilename:  '[name]-[chunkhash].js',
    publicPath:     publicPath
  },

  resolve: {
    root: [path.resolve('./src'), path.resolve('.')],
    extensions: ['', '.jsx', '.css', '.scss', '.js', '.json'],
    modulesDirectories: [
      'node_modules',
      path.resolve(__dirname, './node_modules')
    ],
    alias: {
      react: path.resolve('./node_modules/react'),
      'react-dom': path.resolve('./node_modules/react-dom')
    },
    packageMains: ['browser', 'web', 'browserify', 'main', 'style']
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: { presets: ['es2015', 'react'] },
        plugins: ['transform-function-bind']
      },

      {
        test: /(\.scss|\.css)$/,
        exclude: [],
        loader: ExtractTextPlugin.extract(...cssLoaders)
      }
    ]
  },

  postcss: [autoprefixer],

  sassLoader: {
    data: '@import "' + path.resolve(__dirname, 'theme/_config.scss') + '";'
  },

  plugins: [
    new ExtractTextPlugin('[name]-[hash].css')
  ].concat(plugins),

  devtool: devtool
}


