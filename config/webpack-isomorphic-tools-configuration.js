import { style_loader_filter, style_loader_path_extractor, css_modules_loader_parser } from 'webpack-isomorphic-tools/plugin'

export default
{
  webpack_assets_file_path: 'src/webpack-assets.json',
  webpack_stats_file_path: 'src/webpack-stats.json',

  assets: {

    styles: {
      extensions: ['less', 'scss', 'css'],

      filter(module, regex, options, log) {
        if (options.development)
          return style_loader_filter(module, regex, options, log)

        return regex.test(module.name)
      },

      path(module, options, log) {
        if (options.development)
          return style_loader_path_extractor(module, options, log)

        return module.name
      },

      parser(module, options, log) {
        if (options.development)
          return css_modules_loader_parser(module, options, log)

        return module.source
      }

    },

    images: {
      extensions: ['png', 'jpg', 'gif', 'ico', 'svg']
    }
  }
}
