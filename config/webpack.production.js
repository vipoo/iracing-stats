import webpack from 'webpack'
import WebpackIsomorphicToolsPlugin from 'webpack-isomorphic-tools/plugin'
import WITPConfig from './webpack-isomorphic-tools-configuration'

export const publicPath = '/assets/'

export const cssLoaders = ['style-loader', 'css-loader?modules!postcss-loader!sass-loader']

export const plugins = [
            new WebpackIsomorphicToolsPlugin(WITPConfig),
            new webpack.optimize.UglifyJsPlugin(),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.DedupePlugin(),
            new webpack.DefinePlugin({ 'process.env': { NODE_ENV: '"production"' } }) ]

export const devtool = undefined
