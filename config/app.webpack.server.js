import express from 'express'
import webpack from 'webpack'
import config from './webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'

const compiler = webpack(config)
const port = 8080

const serverOptions = {
  contentBase: `http://localhost:${port}`,
  quiet: false,
  noInfo: false,
  hot: false,
  inline:false,
  lazy: false,
  publicPath: config.output.publicPath,
  headers: {'Access-Control-Allow-Origin': '*'},
  stats: {
    colors: true,
    chunkModules: false,
    source: false,
    chunks: false,
    modules: false,
    assets: true,
    children: false
 }
}

const app = express()

app.use(webpackDevMiddleware(compiler, serverOptions))

app.listen(port, err => err
  ? console.error(err)  // eslint-disable-line no-console
  : console.info('==> 🚧  Webpack development server listening on port %s', port)) // eslint-disable-line no-console


