import morgan from 'morgan'

const morganFormat = ':date[iso] :req[x-forwarded-for] :remote-user :method :url :status - :req[content-length] - :res[content-length] - :response-time ms'

const accessLogs = {
  write: data => console.log(data.slice(0, data.length-1)) // eslint-disable-line no-console
}

export default morgan(
  morganFormat, {
    stream: accessLogs,
    skip: (req) => req.path === '/health'
  })
