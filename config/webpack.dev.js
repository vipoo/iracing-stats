import WebpackIsomorphicToolsPlugin from 'webpack-isomorphic-tools/plugin'
import WITPConfig from './webpack-isomorphic-tools-configuration'

export const publicPath = 'http://localhost:8080/assets/'

export const cssLoaders = ['style-loader?sourceMap', 'css-loader?modules&sourceMap!postcss-loader!sass-loader?sourceMap']

export const plugins = [ new WebpackIsomorphicToolsPlugin(WITPConfig).development() ]

export const devtool = 'cheap-module-eval-source-map'
